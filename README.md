# sieveweb

A simple web based sieve editor and validator for Dovecot IMAP
servers using Dovecot's doveadm API via HTTP.

## Requirements

 * python3 with requests and mako
 * Dovecot IMAP server
 * (apache) web server

## Dovecot config

```
# doveadm HTTP+JSON interface
service doveadm {
   inet_listener http {
      port = 8080
      address = localhost
   }
}
# security: we only allow specific doveadm commands via doveadm-server
#           and only when coming from localhost
# additionally we set doveadm_api_key and disable doveadm_password
# (for localhost) to only allow connections with that API key and not
# with the doveadm_password that is required to talk to the backends
doveadm_allowed_commands = 
local 127.0.0.1/8 {
   doveadm_allowed_commands = user,sieve list,sieve get,sieve put,sieve activate
   doveadm_password =
}
doveadm_api_key = secret-api-key # set this key in sieve_config.py
```

## Apache config

 * standard user interface
 ```
    ScriptAlias /sieve-edit /path/to/sieveweb/sieve-edit.py
    <Location /sieve-edit>
        AuthName "Sieve Editor"
        AuthType Basic
        # any kind of user authentication, e.g. ldap:
        AuthBasicProvider ldap
        AuthLDAPURL "ldap://ldap servers/ou=users,dc=ldap,dc=base??one"
        Require valid-user
    </Location>
 ```
 * admin interface
 ```
    ScriptAlias /sieve-admin /path/to/sieveweb/sieve-admin.py
    <Location /sieve-admin>
        AuthName "Sieve Admin"
        AuthType Basic
        # limit access somehow, e.g. ldap auth or require ip
        AuthBasicProvider ldap
        AuthLDAPURL "ldap://ldap servers/ou=users,dc=ldap,dc=base??one"
        Require user sieveadmin
    </Location>
 ```

## See further

See https://people.mpi-klsb.mpg.de/~pcernko/sieveweb.html for further
details.
