#!/usr/bin/python3

import sys, os
sys.path.append(os.path.dirname(sys.path[0]))
from sieve import SieveWeb

sieve = SieveWeb(admin_mode=False)
sieve.run()
