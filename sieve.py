#!/usr/bin/python3

import base64
import requests
import pprint
import cgi
import os
import re
import traceback
import tempfile
import subprocess
import shutil
import sys
import mako.template
import html

# inspired from https://github.com/hnsk/doveadm-http-cli/blob/master/doveadm/httpclient.py
class DoveAdmHTTPClient(object):
    """ Client for accessing Dovecot Doveadm HTTP API """
    def __init__(self, apiurl, apikey=None, user=None, password=None):
        self.user = user
        self.password = password
        self.apiurl = apiurl
        self.apikey = apikey
        self.errors = {
            64: 'Incorrect parameters',
            65: 'Data error',
            67: 'User does not exist in userdb',
            68: 'Not found',
            73: 'Cannot create file, user out of quota',
            77: 'Authentication failure / permission denied',
            78: 'Invalid configuration'
            }
        self.reqs = requests.Session()
        if self.password:
            self.reqs.auth = (self.user, self.password)
        if self.apikey:
            self.reqs.headers.update({'Authorization': 'X-Dovecot-API '+ base64.b64encode(self.apikey.encode()).decode('ascii')})

    def run_command(self, command, parameters):
        """ Run command with parameters """
        req = self.reqs.post(self.apiurl, json=[[command, parameters, "c01"]])
        if req.status_code == 200:
            return req.json()
        raise Exception("HttpError %s (command=%s parameters=%s)" % (req.status_code, command, parameters))

    # inspired from https://github.com/hnsk/doveadm-http-cli/blob/master/doveadm/httpclient.py
    def parse_response(self, response, command, parameters):
        """ Read Doveadm HTTP API response and print output/error. """
        if response:
            if response[0][0] == "error":
                if response[0][1]['type'] == 'exitCode':
                    exit_code = response[0][1]['exitCode']
                    if exit_code in self.errors:
                        raise Exception(self.errors[exit_code] + " (exitCode: " + str(exit_code) + ") (command=%s parameters=%s)" % (command, parameters))
                    else:
                        raise Exception("Unknown error " + str(exit_code) + " occurred (command=%s parameters=%s)" % (command, parameters))
                else:
                    raise Exception("API error: %s (command=%s parameters=%s)" % (response[0][1], command, parameters))
            else:
                return response[0][1]

    # own code
    def command(self, command, parameters):
        raw_response = self.run_command(command, parameters)
        response = self.parse_response(raw_response, command, parameters)
        return response


class SieveWeb(object):
    def __init__(self, admin_mode=False):
        self.admin = admin_mode
        self.doveadm = None
        self.username = None
        self.ruleset = None
        self.sievescript = None
        self.emailcontent = ""

    def readconfig(self):
        sys.path.append(os.path.dirname(sys.path[0]))
        import sieve_config
        self.apiurl  = sieve_config.apiurl
        try:
            self.apikey  = sieve_config.apikey
            self.apipass = None
            self.apiuser = None
        except AttributeError:
            self.apiuser = sieve_config.user
            self.apipass = sieve_config.password
        try:
            self.director_tag_match = sieve_config.director_tag_match
        except AttributeError:
            self.director_tag_match = None
        try:
            self.sieve_test_path = sieve_config.sieve_test_path
        except AttributeError:
            self.sieve_test_path = 'sieve-test'

    def run(self):
        try:
            self.readconfig()
            self.doveadm = DoveAdmHTTPClient(apiurl=self.apiurl, apikey=self.apikey, user=self.apiuser, password=self.apipass)
            # "bug" in cgi module: we have to force iso encoding as this is the used content-encoding according to RFC, but cgi defaults to UTF-8 in our case
            form = cgi.FieldStorage(encoding='iso-8859-1')
            if not self.admin:
                if not 'REMOTE_USER' in os.environ:
                    self.error('No REMOTE_USER set!')
                self.username = os.environ['REMOTE_USER']
            else:
                if 'user' in form:
                    self.username = form.getvalue('user')
                else:
                    self.userlist()
                    return
            if not self.is_imap_user(self.username):
                raise Exception('User %s not on IMAP backend, no sieve support.' % self.username)
    
            if 'newruleset' in form:
                self.newruleset(form.getvalue('newruleset'))
            elif 'activate' in form:
                self.activateruleset(form.getvalue('activate'))
            elif 'ruleset' in form:
                self.ruleset = form.getvalue('ruleset')
                if 'submit' in form:
                    submittype = form.getvalue('submit')
                    if not 'sievescript' in form:
                        raise Exception('Form submission without sievescript data!')
                    self.sievescript = form.getvalue('sievescript')
                    self.emailcontent = form.getvalue('email', "")
                    if submittype == "Save":
                        self.savesieve()
                    elif submittype == "Test":
                        self.testsieve()
                    else:
                        raise Exception("Illegal submit type: %s" % submittype)
                else:
                    self.editsieve()
            else:
                self.rulesets()
        except Exception as e:
            msg = str(e)
            if self.admin:
                msg += "\n" + traceback.format_exc()
            self.error(msg)

    def is_imap_user(self, user):
        userinfo = self.doveadm.command('user', {'userMask': user})
        if self.director_tag_match is None:
            return True
        if 'director_tag' not in userinfo[user]:
            return False
        if re.search(self.director_tag_match, userinfo[user]['director_tag']):
            return True
        return False

    def template(self, name):
        return mako.template.Template(filename='sieve-templates/%s.html' % name)

    def userlist(self):
        users = self.doveadm.command('user', {'userMask': '*'})
        imapusers = []
        for u in sorted(users['userList']):
            #toslow:#if not self.is_imap_user(u): continue
            # we assume our usernames to be free of html special symbols!
            imapusers.append({
                'text': u,
                'link': '?user=%s' % u
            })

        return self.render_page(
            'User List',
            self.template('userlist').render_unicode(
                userlist=imapusers
            ))

    def newruleset(self, newruleset):
        sieveList = self.doveadm.command('sieveList', {'user': self.username})
        if re.search(r'[^a-zA-Z0-9-]', newruleset):
            raise Exception('Ruleset names must only contain letters, digits and &quot;-&quot;!')
        for ruleset in sieveList:
            if ruleset['script'] == newruleset:
                raise Exception('Ruleset %s already exists!' % newruleset)
        sievePut = self.doveadm.command('sievePut', {'user': self.username, 'scriptname': newruleset, 'file': ''})
        
        # fix GET URL with redirect
        print('Location: ?user=%s' % self.username)
        print('')
        # no page display here, only redirecting

    def activateruleset(self, activate):
        sieveList = self.doveadm.command('sieveList', {'user': self.username})
        found_ruleset = False
        for ruleset in sieveList:
            if ruleset['script'] == activate:
                found_ruleset = True
                if ruleset['active']:
                    raise Exception('Ruleset %s already active!' % activate)
        if not found_ruleset:
            raise Exception('Ruleset %s not found, cannot be activated!' % activate)
        sieveActivate = self.doveadm.command('sieveActivate', {'user': self.username, 'scriptname': activate})
        
        # fix GET URL with redirect
        print('Location: ?user=%s' % self.username)
        print('')
        # no page display here, only redirecting

    def rulesets(self):
        sieveList = self.doveadm.command('sieveList', {'user': self.username})
        rulesets = []
        no_active_ruleset = 'Warning: no active ruleset!'
        for ruleset in sorted(sieveList, key=lambda x: x['script']):
            rshtmlname = html.escape(ruleset['script'])
            rs = {
                'text': rshtmlname,
                'link': '?user=%s&ruleset=%s' % (self.username, rshtmlname),
                'active': False,
                'activatelink': '?user=%s&activate=%s' % (self.username, rshtmlname),
            }
            if ruleset['active'] == 'ACTIVE':
                no_active_ruleset = ''
                rs['active'] = True
            rulesets.append(rs)

        return self.render_page(
            'Rulesets',
            self.template('rulesets').render_unicode(
                username = self.username,
                rulesets = rulesets,
                no_active_ruleset = no_active_ruleset
            ))
            
    def savesieve(self):
        sievePut = self.doveadm.command('sievePut', {'user': self.username, 'scriptname': self.ruleset, 'file': self.sievescript})
        return self.editsieve(content_saved=True)

    def editsieve(self, content_saved=False, test_result=""):
        if not test_result:
            sieveGet = self.doveadm.command('sieveGet', {'user': self.username, 'scriptname': self.ruleset})
            # we have to encode as ascii with xmlcharrefreplace and then decode to STRING again
            self.sievescript = sieveGet[0]['sieve script']

        save_msg = ''
        if content_saved:
            save_msg = 'Saved successfully!'

        return self.render_page(
            'Edit ruleset %s' % self.ruleset,
            self.template('edit').render_unicode(
                username = self.username,
                rulesetname = html.escape(self.ruleset),
                sievescript = html.escape(self.sievescript),
                emailcontent = html.escape(self.emailcontent),
                save_msg = save_msg,
                test_result = html.escape(test_result)
            ))

    def testsieve(self):
        tmpdir = tempfile.mkdtemp(prefix='sieve-edit.')
        scriptfile = tmpdir+'/sieve-script'
        with open(scriptfile, mode='w', encoding='UTF-8') as f:
            f.write(self.sievescript)
        emailfile  = tmpdir+'/email'
        with open(emailfile, mode='w', encoding='UTF-8') as f:
            f.write(self.emailcontent)
        proc = subprocess.Popen(
            [
                self.sieve_test_path,
                '-t-',
                '-Tlevel=matching',
                '-omail_uid=',
                '-omail_gid=',
                scriptfile,
                emailfile
            ],
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        stdout, stderr = proc.communicate('')
        returncode = proc.returncode
        test_result = ""
        if returncode == 0:
            test_result = stdout.decode()
        else:
            test_result = "<b>Error</b>: %s\n\n%s" % (stderr.decode(), stdout.decode())
        shutil.rmtree(tmpdir, ignore_errors=True)
        return self.editsieve(test_result=test_result)

    def error(self, msg):
        return self.render_page(
            'Error',
            self.template('error').render_unicode(
                msg = msg
            ))

    def render_page(self, title, content):
        pagetitle = "Sieve-Edit: "
        if self.admin:
            pagetitle = "Sieve-Admin: "
        pagetitle += title
        page = self.template('main').render_unicode(
            pagetitle = pagetitle,
            pagecontent = content
        ).encode('ascii', 'xmlcharrefreplace').decode()
        print('Content-Type: text/html')
        print('')
        print(page)
